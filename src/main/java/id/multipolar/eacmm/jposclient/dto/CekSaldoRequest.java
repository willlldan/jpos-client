package id.multipolar.eacmm.jposclient.dto;

import lombok.Data;

@Data
public class CekSaldoRequest {

	private String pan;
	private String termId;
	private String CANLoc;
	private String kodeBank;
	
	
}
