package id.multipolar.eacmm.jposclient.controller;

import java.util.Iterator;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.BASE24Packager;
import org.jpos.iso.packager.ISO87APackager;
import org.springframework.util.SocketUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/topup")
public class TopUpController {

	@PostMapping
	public String topUp(@RequestBody String isoMsg) throws ISOException {

		ISOMsg request = new ISOMsg();
		request.setPackager(new BASE24Packager());
		request.unpack(isoMsg.getBytes());

		Long amount = Long.valueOf(request.getString(4));
		Long fee = Long.valueOf(request.getString(28));
		String bit48 = request.getString(48);
		String name = bit48.substring(0, 31).trim();
		String konter = bit48.substring(32, 64).trim();
		
		String amountPrint = String.format("Rp. %,d", amount).replace(",", ".");
		String feePrint = String.format("Rp. %,d", fee).replace(",", ".");
		
		
		String output = String.format("Kamu telah membeli pulsa telkomsel sebesar %s dengan biaya admin %s atas nama %s pada konter %s terimakasih", amountPrint, feePrint, name, konter);
		
		
		return output;
	}
}
