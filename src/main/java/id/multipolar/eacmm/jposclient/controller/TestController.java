package id.multipolar.eacmm.jposclient.controller;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.MUX;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
public class TestController {

	@Autowired
	private MUX mux;
	

	@GetMapping("/test")
	public String test() throws ISOException {
		ISOMsg msg = new ISOMsg();
		msg.setMTI("0800");
		msg.set(11, "000001");
		msg.set(70, "301");
		
		ISOMsg respMsg =  mux.request(msg, 3000);
		return respMsg.toString();
		
	}
}
