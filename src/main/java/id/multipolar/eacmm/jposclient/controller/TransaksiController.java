package id.multipolar.eacmm.jposclient.controller;

import java.util.HashMap;
import java.util.Map;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.q2.iso.QMUX;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.multipolar.eacmm.jposclient.dto.CekSaldoRequest;

@RestController
@RequestMapping("/api/transaksi")
public class TransaksiController {
	
	@Autowired
	private QMUX qmux;

	@PostMapping("/cek-saldo")
	public Map<String, String> cekSaldo(@RequestBody CekSaldoRequest request) throws ISOException {
		Map<String, String> result = new HashMap<String, String>();
		
		ISOMsg msgRequest = new ISOMsg("0200");
		msgRequest.set(2, request.getPan());
		msgRequest.set(3, "380242");
		msgRequest.set(41, request.getTermId());
		msgRequest.set(43, request.getCANLoc());
		msgRequest.set(127, request.getKodeBank());
		
		ISOMsg isoResponse = qmux.request(msgRequest, 20 * 1000);
		
		if (isoResponse == null) {
			result.put("success", "false");
			result.put("error", "timeour");
		}
		
		String response = new String(isoResponse.pack());
		
		result.put("success", "true");
		result.put("response_code", isoResponse.getString(39));
		result.put("raw_message", response);
		
		return result;
		
	}
}
